import requests

def get_course():
    offset = 2.3
    raw = requests.get('https://api.nbrb.by/exrates/rates/431')
    raw_course = raw.json()['Cur_OfficialRate']
    percents = raw_course /100 * offset
    course = raw_course + percents
    #print(course)
    return course

if __name__ == "__main__":
    print(get_course())