import re
import requests
from bs4 import BeautifulSoup

def get_prices():
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    html_doc = requests.get("https://www.united-company.by/azs/tseny-na-toplivo/", headers=headers).text

    soup = BeautifulSoup(html_doc, 'html.parser')

    trs = soup.find_all("tr")
    #print(html_doc)


    price_regex = r"[\d]{1,5}[.][\d]{2}"

    full_list = []
    #print(trs)
    name = ""
    price = ""
    dct = {}

    for tr in trs:
        for c, i in enumerate(tr.find_all("td")[:2]):
            #print(i, "\n--------------------------")
            raw = i.contents[0]
            price = re.findall(price_regex, raw)

            if c%2 == 0:
                name = str(i).replace("Бензин", "").replace("<td>", "").replace("</td>", "").replace("<b>", "").replace('</b>', "").replace(" ","").replace('\t', '').replace('\n', '')
                continue

            if name:
                dct[name] = float(price[0])
                name = ""

            if len(price) > 0:
                full_list.append(price)
    return dct

if __name__ == "__main__":
    print(get_prices())