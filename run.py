#!/usr/bin/env python3
from get_nbrb_course import get_course
from get_prices import get_prices

hardware_price = 400

prices = get_prices()
price_gas = prices["Газ"]
price_92 = prices["АИ-92"]

consumption = 12
course = get_course()

print(f"92 price: {price_92}/{round(price_92/course,2)}$\nlpg price: {price_gas}/{round(price_gas/course,2)}$")

over_cunsumption = consumption + consumption /100 * 15 #percents
milege = 5000
diff = 0

#print("goto while")
while diff != hardware_price:
    milege += 1
    price_benz_sum = ((milege / 100) * (consumption * price_92))/course         #(milege / 100km) * (consumption * price_92)
    price_gas_sum = ((milege / 100) * (over_cunsumption * price_gas))/course
    diff = int(price_benz_sum-price_gas_sum)

print("benz: %s$" %(round(price_benz_sum, 1)))
print("lpg: %s$" %(round(price_gas_sum, 1)))

print("milege: %dkm\nconsumption_benz: %dL/100km\nconsumption_lpg: %dL/100km\ndiff: %d$" %(milege, consumption, over_cunsumption, round(price_benz_sum-price_gas_sum, 1)))